let config = {
	lintOnSave: false,
	publicPath: './',
	devServer: {
		hotOnly: true,
		open: false
	},
	// css: {
	// 	sourceMap: false,
	// 	loaderOptions: {
	// 		sass: {
	// 			prependData: `
	// 									@import "@/assets/scss/base.scss";
	// 								`
	// 		}
	// 	}
	// }
}

module.exports = config;