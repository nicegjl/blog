const routes = [
	{ path: '/', name: 'Index', component: resolve => require(['@/views/index'], resolve) }
]

export default routes;